<?php
    //panggil file yang berisi oeperasi db
    require_once 'db/class_kegiatan.php';
    //buat variabel untuk melakukan operasi db
    $obj = new Kegiatan();
    //buat variabel untuk mengambil data dari form dan menyimpannya dlm array
    $_tanggal = $_POST['tanggal'];
    $_nilai = $_POST['nilai'];
    $_tempat = $_POST['tempat'];
    $_tugasakhir_id = $_POST['tugasakhir_id'];
    $_dosen_penguji = $_POST['dosen_penguji'];
    $_kategori_seminar_id = $_POST['kategori_seminar_id'];

    $_proses = $_POST['proses'];

    $ar_data[] = $_tanggal;
    $ar_data[] = $_nilai;
    $ar_data[] = $_tempat;
    $ar_data[] = $_tugasakhir_id;
    $ar_data[] = $_dosen_penguji;
    $ar_data[] = $_kategori_seminar_id;
    //buat operasi jika memilih button simpan, update atau hapus
    $row = 0;
    if($_proses == "Simpan"){
        $row = $obj->simpan($ar_data);
    }elseif($_proses == "Update"){
        $_idedit = $_POST['idedit'];
        $ar_data[] = $_idedit;
        $row = $obj->ubah($ar_data);
    }elseif($_proses == "Hapus"){
        unset($ar_data);
        $_idedit = $_POST['idedit'];
        $row = $obj->hapus($_idedit);
    }
    //handeler jika gagal atau sukses
    if($row==0){
        echo "Gagal Proses";
    }else{
        //echo "Proses Sukses";
        //langsung direct ke daftar_kegiatan.php
        header('Location:index.php');
    }
?>
