<?php
    /*
    mysql> desc seminar_ta;
+---------------------+-------------+------+-----+---------+----------------+
| Field               | Type        | Null | Key | Default | Extra          |
+---------------------+-------------+------+-----+---------+----------------+
| id                  | int(11)     | NO   | PRI | NULL    | auto_increment |
| tanggal             | date        | YES  |     | NULL    |                |
| nilai               | double      | YES  |     | NULL    |                |
| tempat              | varchar(45) | YES  |     | NULL    |                |
| tugasakhir_id       | int(11)     | NO   | MUL | NULL    |                |
| dosen_penguji       | int(11)     | NO   | MUL | NULL    |                |
| kategori_seminar_id | int(11)     | YES  |     | NULL    |                |
+---------------------+-------------+------+-----+---------+----------------+
7 rows in set (0,01 sec)



    */
    require_once "DAO.php";
    class Kegiatan extends DAO
    {
        public function __construct()
        {
            parent::__construct("seminar_ta");
        }

        public function simpan($data){
            $sql = "INSERT INTO ".$this->tableName.
            " (id,tanggal,nilai,tempat,tugasakhir_id,dosen_penguji,kategori_seminar_id) ".
            " VALUES (default,?,?,?,?,?,?)";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }

        public function ubah($data){
            $sql = "UPDATE ".$this->tableName.
            " SET tanggal=?,nilai=?,tempat=?,tugasakhir_id=?,dosen_penguji=?,kategori_seminar_id=? ".
            " WHERE id=?";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }
        //buat fungsi untuk menampilkan statistik

    }
?>
