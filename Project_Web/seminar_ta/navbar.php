<body>

    <!-- Side Menu -->
    <a id="menu-toggle" href="#" class="btn btn-primary btn-lg toggle"><i class="fa fa-bars"></i></a>
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <li>
                <a id="menu-close" href="#" class="btn btn-default btn-lg pull-right toggle">
                    <i class="fa fa-times"></i>
                </a>
            </li>
            <li class="sidebar-brand">
                <a href="../index.html">STT NF</a>
                <hr>
            </li>
            <li>
                <a href="../tugasakhir/index.php">Daftar Tugas Akhir</a>
            </li>
            <li>
                <a href="../kategori_seminar/index.php">Kategori Seminar</a>
            </li>
            <li>
                <a href="index.php">Daftar Seminar TA</a>
            </li>
            <li>
                <a href="../tugasakhir/grafik_kegiatan.php">Grafik Nilai TA</a>
            </li>
        </ul>
    </div>
    <!-- /Side Menu -->

    <!-- Full Page Image Header Area -->
    <div id="myCarousel" class="carousel carousel-fade slide" data-ride="carousel" data-interval="2500">
      <div class="carousel-inner" role="listbox">
        <div class="item active background a"></div>
        <div class="item background b"></div>
        <div class="item background c"></div>
      </div>
    </div>

    <div class="covertext">
      <div class="col-lg-10" style="float:none; margin:0 auto;">
        <h3 class="title">Daftar Seminar Tugas Akhir</h3>
        <h2 class="subtitle">STT TERPADU NURUL FIKRI.</h2>
      </div>
      <div class="col-xs-12 explore">
        <a href="#example" class="btn header-btn">Find Out More</a>
      </div>
    </div>
    <!-- /Full Page Image Header Area -->
