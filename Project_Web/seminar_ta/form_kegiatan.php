<?php
    include_once 'top.php';
    //panggil file yang melakukan operasi db
    require_once 'db/class_kegiatan.php';
    //buat variabel untuk memanggil class
    $obj_kegiatan = new Kegiatan();
    //buat variabel utk menyimpan id
    $_idedit = $_GET['id'];
    //buat pengecekan apakah datanya ada atau tidak
    if(!empty($_idedit)){
        $data = $obj_kegiatan->findByID($_idedit);
    }else{
        $data = [];
    }
?>
<script src="js/form_validasi_kegiatan.js"></script>
<form name="form_kegiatan" class="form-horizontal" method="POST" action="proses_kegiatan.php">
<fieldset>

<!-- Form Name -->
<legend>Form Name</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="tanggal">tanggal</label>
  <div class="col-md-4">
  <input id="tanggal" name="tanggal" type="text" placeholder="Masukkan Tanggal" class="form-control input-md" value="<?php echo $data['tanggal']?>">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nilai">nilai</label>
  <div class="col-md-4">
  <input id="nilai" name="nilai" type="text" placeholder="Masukkan Nilai" class="form-control input-md" value="<?php echo $data['nilai']?>" >

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="tempat">tempat</label>
  <div class="col-md-4">
  <input id="tempat" name="tempat" type="text" placeholder="Masukkan Tempat" class="form-control input-md" value="<?php echo $data['tempat']?>">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="tugasakhir_id">ID Tugas Akhir</label>
  <div class="col-md-4">
  <input id="tugasakhir_id" name="tugasakhir_id" type="text" placeholder="tugas akhir" class="form-control input-md" value="<?php echo $data['tugasakhir_id']?>">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="dosen_penguji">dosen penguji</label>
  <div class="col-md-4">
  <input id="dosen_penguji" name="dosen_penguji" type="text" placeholder="Dosen Penguji" class="form-control input-md" value="<?php echo $data['dosen_penguji']?>">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="kategori_seminar_id">ID Kategori Seminar</label>
  <div class="col-md-4">
  <input id="kategori_seminar_id" name="kategori_seminar_id" type="text" placeholder="ID Kategori Seminar" class="form-control input-md" value="<?php echo $data['kategori_seminar_id']?>">

  </div>
</div>



<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="proses"></label>
  <div class="col-md-8">
  <?php
    if(empty($_idedit)){
    ?>
      <input type="submit" name="proses" class="btn btn-success" value="Simpan"/>
    <?php
    }else{
      ?>
      <input type="hidden" name="idedit" value="<?php echo $_idedit?>"/>
      <input type="submit" name="proses" class="btn btn-primary" value="Update"/>
      <input type="submit" name="proses" class="btn btn-danger" value="Hapus"/>
    <?php
    }?>
  </div>
</div>
</fieldset>
</form>

<?php
    include_once 'bottom.php';
?>
