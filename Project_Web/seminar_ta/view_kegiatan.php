<?php
    include_once 'top.php';
    require_once 'db/class_kegiatan.php';
    //panggil file untuk operasi db
    //buat variabel utk menyimpan id
    //buat variabel untuk mengambil id
    $objKegiatan = new Kegiatan();
    $_id = $_GET['id'];
    $data = $objKegiatan->findByID($_id);
?>
<!--Buat tampilan dengan tabel-->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">View Kegiatan</h3>
            </div>
            <div class="panel-body">
                <table class="table">
                <tr>
                <td class="active">ID</td><td>:</td><td><?php echo
                $data['id']?></td>
                </tr>
                <tr>
                <td class="active">Tanggal</td><td>:</td><td><?php echo
                $data['tanggal']?></td>
                </tr>
                <tr>
                <td class="active">Nilai</td><td>:</td><td><?php echo
                $data['nilai']?></td>
                </tr>
                <tr>
                <td class="active">Tempat</td><td>:</td><td><?php
                echo $data['tempat']?></td>
                </tr>
                <tr>
                <td class="active">ID Tugas Akhir</td><td>:</td><td><?php echo
                $data['tugasakhir_id']?></td>
                </tr>
                <tr>
                <td class="active">Dosen Penguji</td><td>:</td><td><?php echo
                $data['dosen_penguji']?></td>
                </tr>
                <tr>
                <td class="active">ID Kategori Seminar</td><td>:</td><td><?php echo
                $data['kategori_seminar_id']?></td>
                </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<?php
    include_once 'bottom.php';
?>
