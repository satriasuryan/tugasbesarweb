<?php
    include_once 'top.php';
    require_once 'db/class_kegiatan.php';
    //panggil file untuk operasi db
    //buat variabel utk menyimpan id
    //buat variabel untuk mengambil id
    $objKegiatan = new Kegiatan();
    $_id = $_GET['id'];
    $data = $objKegiatan->findByID($_id);
?>
<!--Buat tampilan dengan tabel-->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">View Kegiatan</h3>
            </div>
            <div class="panel-body">
                <table class="table">
                <tr>
                <td class="active">ID</td><td>:</td><td><?php echo
                $data['id']?></td>
                </tr>
                <tr>
                <td class="active">Judul</td><td>:</td><td><?php echo
                $data['judul']?></td>
                </tr>
                <tr>
                <td class="active">Semester</td><td>:</td><td><?php echo
                $data['semester']?></td>
                </tr>
                <tr>
                <td class="active">Nim</td><td>:</td><td><?php
                echo $data['nim']?></td>
                </tr>
                <tr>
                <td class="active">Dosen Pembimbing</td><td>:</td><td><?php echo
                $data['dosen_pembimbing']?></td>
                </tr>
                <tr>
                <td class="active">Nilai</td><td>:</td><td><?php echo
                $data['nilai']?></td>
                </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<?php
    include_once 'bottom.php';
?>
