<?php
include_once 'top.php';
require_once 'db/class_kegiatan.php';

  $obj = new Kegiatan();// buat instan objek class Kegiatan
  $rs = $obj->getStatistik(); // panggil method query
  $ar_data = [];// siapkan array kosong
  foreach($rs as $row){
  $ar['label']=$row['nim'];// buat array key label
  $ar['y']=(int)$row['nilai']; // buat array key y
  $ar_data[]=$ar; // masukan array ke ar_data
  }
  $out = array_values($ar_data); // ubah format menjadi array_values

?>
    <script type="text/javascript">

    window.onload = function () {

      var chart = new CanvasJS.Chart("chartContainer", {
        theme: "light1", // "light2", "dark1", "dark2"
        animationEnabled: true, // change to true
        title:{
          text: "Grafik Nilai Tugas Akhir Mahasiswa"
        },
        data: [
          {
            // Change type to "bar", "area", "spline", "pie",etc.
            type: "pie",
            dataPoints:<?php echo json_encode($out) ?>
          }
        ]
      });

      chart.render();

      }
      </script>
      </head>
      <body>

      <div id="chartContainer" style="height: 370px; width: 100%;"></div>
      <script src="https://canvasjs.com/assets/script/canvasjs.min.js">
      </script>

      <?php
      include_once 'bottom.php';
      ?>
