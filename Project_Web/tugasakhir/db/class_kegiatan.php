<?php
    /*
    mysql> desc tugasakhir;
+------------------+-------------+------+-----+---------+----------------+
| Field            | Type        | Null | Key | Default | Extra          |
+------------------+-------------+------+-----+---------+----------------+
| id               | int(11)     | NO   | PRI | NULL    | auto_increment |
| judul            | text        | YES  |     | NULL    |                |
| semester         | int(11)     | YES  |     | NULL    |                |
| nim              | varchar(10) | NO   | MUL | NULL    |                |
| dosen_pembimbing | int(11)     | NO   | MUL | NULL    |                |
| nilai            | double      | YES  |     | NULL    |                |
+------------------+-------------+------+-----+---------+----------------+
6 rows in set (0,00 sec)


    */
    require_once "DAO.php";
    class Kegiatan extends DAO
    {
        public function __construct()
        {
            parent::__construct("tugasakhir");
        }

        public function simpan($data){
            $sql = "INSERT INTO ".$this->tableName.
            " (id,judul,semester,nim,dosen_pembimbing,nilai) ".
            " VALUES (default,?,?,?,?,?)";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }

        public function ubah($data){
            $sql = "UPDATE ".$this->tableName.
            " SET judul=?,semester=?,nim=?,dosen_pembimbing=?,nilai=? ".
            " WHERE id=?";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }
        //buat fungsi untuk menampilkan statistik
        public function getStatistik(){
        $sql = "SELECT tugasakhir.nim, tugasakhir.nilai as nilai from seminar_ta
        LEFT JOIN tugasakhir ON tugasakhir.id= seminar_ta.tugasakhir_id
        GROUP BY tugasakhir.id" ;
        $ps = $this->koneksi->prepare($sql);
        $ps->execute();
        return $ps->fetchAll();
 }
    }
?>
