<?php
    //panggil file yang berisi oeperasi db
    require_once 'db/class_kegiatan.php';
    //buat variabel untuk melakukan operasi db
    $obj = new Kegiatan();
    //buat variabel untuk mengambil data dari form dan menyimpannya dlm array
    $_judul = $_POST['judul'];
    $_semester = $_POST['semester'];
    $_nim = $_POST['nim'];
    $_dosen_pembimbing = $_POST['dosen_pembimbing'];
    $_nilai = $_POST['nilai'];

    $_proses = $_POST['proses'];

    $ar_data[] = $_judul;
    $ar_data[] = $_semester;
    $ar_data[] = $_nim;
    $ar_data[] = $_dosen_pembimbing;
    $ar_data[] = $_nilai;
    //buat operasi jika memilih button simpan, update atau hapus
    $row = 0;
    if($_proses == "Simpan"){
        $row = $obj->simpan($ar_data);
    }elseif($_proses == "Update"){
        $_idedit = $_POST['idedit'];
        $ar_data[] = $_idedit;
        $row = $obj->ubah($ar_data);
    }elseif($_proses == "Hapus"){
        unset($ar_data);
        $_idedit = $_POST['idedit'];
        $row = $obj->hapus($_idedit);
    }
    //handeler jika gagal atau sukses
    if($row==0){
        echo "Gagal Proses";
    }else{
        //echo "Proses Sukses";
        //langsung direct ke daftar_kegiatan.php
        header('Location:index.php');
    }
?>
