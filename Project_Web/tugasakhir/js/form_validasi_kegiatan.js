//buat fungsi
$(function(){
    //panggil nama form lalu gunakan fungsi validasi
    $("form[name='form_kegiatan']").validate({

        //buat aturan untuk input form
        rules:{
            nilai:{
                required:true,
                maxLength:3,
            },
            judul:"required",
            narasumber:"required",
            deskripsi:"required",
        },
        //tampilkan pesan
        messages:{
            nilai:{
                required:"Nilai Wajib diisi!!",
                maxLength:"Max 3 Character",
            },
            judul:"Judul wajib diisi!!",
            narasumber:"Narasumber wajib diisi",
            deskripsi:"Deskripsi wajib diisi",
        },
        //submit form
        submitHandler:function(form){
            form.submit();
        }
    });

});
