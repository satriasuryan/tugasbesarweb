<?php
    include_once 'top.php';
    //panggil file yang melakukan operasi db
    require_once 'db/class_mahasiswa.php';
    //buat variabel untuk memanggil class
    $obj_mahasiswa = new Mahasiswa();
    //buat variabel utk menyimpan id
    $_idedit = $_GET['id'];
    //buat pengecekan apakah datanya ada atau tidak
    if(!empty($_idedit)){
        $data = $obj_mahasiswa->findByID($_idedit);
    }else{
        $data = [];
    }
?>
<script src="js/form_validasi_mahasiswa.js"></script>
<form class="form-horizontal" method="POST" name="form_mahasiswa" action="proses_mahasiswa.php">
<fieldset>

<!-- Form Name -->
<legend>Form Mahasiswa</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nim">NIM</label>  
  <div class="col-md-4">
  <input id="nim" name="nim" type="text" placeholder="Masukkan NIM" class="form-control input-md" value="<?php echo $data['nim']?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nama">Nama</label>  
  <div class="col-md-4">
  <input id="nama" name="nama" type="text" placeholder="Masukkan Nama" class="form-control input-md" value="<?php echo $data['nama']?>" >
    
  </div>
</div>


<div class="form-group">
  <label class="col-md-4 control-label" for="jk">Jenis Kelamin</label>
  <div class="col-md-4">
    <select id="jk" name="jk" class="form-control">
      <option value="0">Pilih Jenis Kelamin</option>
      <option value="L">Laki-laki</option>
      <option value="P">Perempuan</option>
    </select>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="prodi_id">Prodi ID</label>  
  <div class="col-md-4">
	  <?php
	  $dataprodi = $obj_mahasiswa->getAllProdi();
	  //die(print_r($dataprodi));
	  
	  ?>
	  <select name="prodi_id" class="form-control">
		  <?php
		    foreach($dataprodi as $row){
		     echo'<option value="'.$row['id'].'">'.$row['nama'].'</option>';
		   }
		  ?>
	  </select>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="thnmasuk">Tahun Masuk</label>
  <div class="col-md-4">
  <input id="thnmasuk" name="thnmasuk" type="number" placeholder="Masukkan Tahun Masuk" class="form-control input-md" value="<?php echo $data['thnmasuk']?>" >
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="rombel_id">Rombel ID</label>
  <div class="col-md-4">
	  <?php
	  $datarombel = $obj_mahasiswa->getAllRombel();
	  //die(print_r($dataprodi));
	  
	  ?>
	  <select name="rombel_id" class="form-control">
		  <?php
		    foreach($datarombel as $row){
		     echo'<option value="'.$row['id'].'">'.$row['nama'].'</option>';
		   }
		  ?>
	  </select>
  <input id="rombel_id" name="rombel_id" type="number" placeholder="Masukkan Rombel ID" class="form-control input-md" value="<?php echo $data['rombel_id']?>" >
  </div>
</div>

<!-- Select Basic -->
<!--
<div class="form-group">
  <label class="col-md-4 control-label" for="kategori">Kategori</label>
  <div class="col-md-4">
    <select id="kategori" name="kategori" class="form-control">
      <option value="0">Pilih Kategori</option>
      <option value="1">Seminar</option>
      <option value="2">Workshop</option>
      <option value="3">Training</option>
      <option value="4">Conferences</option>
      <option value="5">Kuliah Umum</option>
    </select>
  </div>
</div>
-->

<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="proses"></label>
  <div class="col-md-8">
  <?php
    if(empty($_idedit)){
    ?>
      <input type="submit" name="proses" class="btn btn-success" value="Simpan"/>
      <a class="btn icon-btn btn-success" href="index_mahasiswa.php">Cancel</a>
    <?php
    }else{
      ?>
      <input type="hidden" name="idedit" value="<?php echo $_idedit?>"/>
      <input type="submit" name="proses" class="btn btn-primary" value="Update"/>
      <input type="submit" name="proses" class="btn btn-danger" value="Hapus"/>
      <a class="btn icon-btn btn-success" href="index.php#a">Cancel</a>
    <?php
    }?>
  </div>
</div>
</fieldset>
</form>

<?php
    include_once 'bottom.php';
?>
