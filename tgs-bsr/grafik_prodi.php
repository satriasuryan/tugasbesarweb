<?php
	include_once 'top.php';
	require_once 'db/class_prodi.php';

$obj = new Prodi();
$rs = $obj->getStatistik();
$ar_data = [];
foreach($rs as $row){
	$ar['label']=$row['nama'];
	$ar['y']=(int)$row['jumlah'];
	$ar_data[]=$ar;
}
$out = array_values($ar_data);
?>

<script type="text/javascript">
window.onload = function() {
	
	var chart = new CanvasJS.Chart("chartContainer", {
		theme: "dark2", // "light2", "dark1", "dark2"
		animationEnabled: true, // change to true
		title:{
			text: "Grafik Statistik Prodi Mahasiswa"
		},
		data: [
		{
			type: "column",
			dataPoints:<?php echo json_encode($out) ?>
		}
		]
	});
chart.render();
}
</script>
</head>
<body>

<div id="chartContainer" style="height: 370px; width: 100%;"></div>
<script src="js/canvasjs.min.js"> 
</script>

<?php
    include_once 'bottom.php'
?>
