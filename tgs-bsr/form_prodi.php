<?php
    include_once 'top.php';
    //panggil file yang melakukan operasi db
    require_once 'db/class_prodi.php';
    //buat variabel untuk memanggil class
    $obj_Prodi = new Prodi();
    //buat variabel utk menyimpan id
    $_idedit = $_GET['id'];
    //buat pengecekan apakah datanya ada atau tidak
    if(!empty($_idedit)){
        $data = $obj_Prodi->findByID($_idedit);
    }else{
        $data = [];
    }
?>
<script src="js/form_validasi_prodi.js"></script>
<form class="form-horizontal" method="POST" name="form_prodi" action="proses_prodi.php">
<fieldset>

<!-- Form Name -->
<legend>Form Prodi</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="kode">Kode</label>  
  <div class="col-md-4">
  <input id="kode" name="kode" type="text" placeholder="Masukkan Kode" class="form-control input-md" value="<?php echo $data['kode']?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nama">Nama Prodi</label>  
  <div class="col-md-4">
  <input id="nama" name="nama" type="text" placeholder="Masukkan Nama Prodi" class="form-control input-md" value="<?php echo $data['nama']?>" >
    
  </div>
</div>

<!-- Select Basic -->
<!--
<div class="form-group">
  <label class="col-md-4 control-label" for="kategori">Kategori</label>
  <div class="col-md-4">
    <select id="kategori" name="kategori" class="form-control">
      <option value="0">Pilih Kategori</option>
      <option value="1">Seminar</option>
      <option value="2">Workshop</option>
      <option value="3">Training</option>
      <option value="4">Conferences</option>
      <option value="5">Kuliah Umum</option>
    </select>
  </div>
</div>
-->


<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="proses"></label>
  <div class="col-md-8">
  <?php
    if(empty($_idedit)){
    ?>
      <input type="submit" name="proses" class="btn btn-success" value="Simpan"/>
      <a class="btn icon-btn btn-success" href="index_prodi.php">Cancel</a>
    <?php
    }else{
      ?>
      <input type="hidden" name="idedit" value="<?php echo $_idedit?>"/>
      <input type="submit" name="proses" class="btn btn-primary" value="Update"/>
      <input type="submit" name="proses" class="btn btn-danger" value="Hapus"/>
      <a class="btn icon-btn btn-success" href="index.php#b">Cancel</a>
    <?php

    }?>
  </div>
</div>
</fieldset>
</form>

<?php
    include_once 'bottom.php';
?>
