<?php
include_once 'top.php';
require_once 'db/class_prodi.php';
?>

<h2>Daftar Prodi</h2>

<?php
$obj_prodi = new Prodi();
$rows = $obj_prodi->getAll();
?>

<table id="example" class="table table-striped table-bordered"><!-- Beri id pada tag table untuk dideteksi javascript-->
    <thead>
    <tr class="active">
        <th>No</th><th>Id Kode</th><th>Kode Prodi</th><th>Nama Prodi</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $nomor = 1;
    foreach($rows as $row){
        echo '<tr><td>'.$nomor.'</td>';
        echo '<td>'.$row['id'].'</td>';
        echo '<td>'.$row['kode'].'</td>';
        echo '<td>'.$row['nama'].'</td>';
        echo '</tr>';
        $nomor++;
    }
    ?>


    </tbody>
</table>

<?php
include_once 'bottom.php';
?>
