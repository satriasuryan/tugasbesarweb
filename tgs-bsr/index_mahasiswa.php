<?php
include_once 'top.php';
require_once 'db/class_mahasiswa.php';
?>


<h2>Daftar Mahasiswa</h2>


<?php
$obj = new Mahasiswa();
$rows = $obj->getAll();
?>

<table id="example" class="table table-striped table-bordered"><!-- Beri id pada tag table untuk dideteksi javascript-->
    <thead>
    <tr class="active">
        <th>No</th><th>NIM</th><th>Nama</th><th>JK</th><th>Prodi id</th><th>Tahun Masuk</th><th>Rombel id</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $nomor = 1;
    foreach($rows as $row){
        echo '<tr><td>'.$nomor.'</td>';
        echo '<td>'.$row['nim'].'</td>';
        echo '<td>'.$row['nama'].'</td>';
        echo '<td>'.$row['jk'].'</td>';
        echo '<td>'.$row['prodi_id'].'</td>';
        echo '<td>'.$row['thnmasuk'].'</td>';
        echo '<td>'.$row['rombel_id'].'</td>';
        echo '</tr>';
        $nomor++;
    }
     
    ?>
    </tbody>
    </table>
<?php
    include_once 'bottom.php';
?>
