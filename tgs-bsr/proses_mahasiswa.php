<?php
    //panggil file yang berisi oeperasi db
    require_once 'db/class_mahasiswa.php';
    //buat variabel untuk melakukan operasi db
    $obj = new Mahasiswa();
    //buat variabel untuk mengambil data dari form dan menyimpannya dlm array
    $_nim = $_POST['nim'];
    $_nama = $_POST['nama'];
    $_jk = $_POST['jk'];
    $_prodi_id = $_POST['prodi_id'];
    $_thnmasuk = $_POST['thnmasuk'];
    $_rombel_id = $_POST['rombel_id'];
    $_proses = $_POST['proses'];


    $ar_data[] = $_nim;
    $ar_data[] = $_nama;
    $ar_data[] = $_jk;
    $ar_data[] = $_prodi_id;
    $ar_data[] = $_thnmasuk;
    $ar_data[] = $_rombel_id;
    
    //buat operasi jika memilih button simpan, update atau hapus
    $row = 0;
    if($_proses == "Simpan"){
        $row = $obj->simpan($ar_data);
    }elseif($_proses == "Update"){
        $_idedit = $_POST['idedit'];
        $ar_data[] = $_idedit;
        $row = $obj->ubah($ar_data);
    }elseif($_proses == "Hapus"){
        unset($ar_data);
        $_idedit = $_POST['idedit'];
        $row = $obj->hapus($_idedit);
    }
    //handeler jika gagal atau sukses
    if($row==0){
        echo "Gagal Proses";
    }else{
        //echo "Proses Sukses";
        //langsung direct ke daftar_kegiatan.php
        header('Location:index.php#a');
    }
?>
