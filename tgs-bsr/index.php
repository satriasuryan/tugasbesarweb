


<?php
include_once 'top.php';
require_once 'db/class_prodi.php';
?>

	<img alt="Bootstrap Image Preview" style="width:100%;height:auto;" src="img/kampusnf.jpg">

<br>

<div class="panel-header">
    <a class="btn icon-btn btn-success" href="#a">View Mahasiswa</a>
    <a class="btn icon-btn btn-success" href="#b">View Prodi</a>
</div>

<br>

<?php
    include_once 'bottom.php';
?>
<br><br><br><br>

<a name="b"></a>
<div class="panel-header">
    <a class="btn icon-btn btn-primary" href="index.php">Home</a>
    
        <a class="btn icon-btn btn-success" href="form_prodi.php">
        <span class="glyphicon btn-glyphicon glyphicon-plus img-circle text-succes"></span>Tambah Prodi</a>

</div>


<h2 align = "center">Daftar Prodi</h2>
<?php
$obj_prodi = new Prodi();
$rows = $obj_prodi->getAll();
?>
<!-- Buat code javascript untuk memanggil table dan menggunakan fungsi datatable-->
<script language="javascript">
        $(document).ready(function() {
        $('#prodi').DataTable();
        });
</script>

<table id="prodi" class="table table-striped table-bordered"><!-- Beri id pada tag table untuk dideteksi javascript-->
    <thead>
    <tr class="active">
        <th>No</th><th>Id Kode</th><th>Kode Prodi</th><th>Nama Prodi</th><th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $nomor = 1;
    foreach($rows as $row){
        echo '<tr><td>'.$nomor.'</td>';
        echo '<td>'.$row['id'].'</td>';
        echo '<td>'.$row['kode'].'</td>';
        echo '<td>'.$row['nama'].'</td>';
        echo '<td><a href="view_prodi.php?id='.$row['id']. '">View</a> |';
        echo '<a href="form_prodi.php?id='.$row['id']. '">Update</a></td>';
        echo '</tr>';
        $nomor++;
    }
    ?>

    </tbody>
</table>


<br><br><br><br><br><br>


<a name="a"></a>

<div class="panel-header">
    <a class="btn icon-btn btn-primary" href="index.php">Home</a>
    
        <a class="btn icon-btn btn-success" href="form_mahasiswa.php">
        <span class="glyphicon btn-glyphicon glyphicon-plus img-circle text-succes"></span>Tambah Mahasiswa</a>

<?php
require_once 'db/class_mahasiswa.php';
?>

<h2 align = "center">Daftar Mahasiswa</h2>

<?php
$obj = new Mahasiswa();
$rows = $obj->getAll();
?>
<!-- Buat code javascript untuk memanggil table dan menggunakan fungsi datatable-->
<script language="javascript">
        $(document).ready(function() {
        $('#example').DataTable();
        });
</script>

<table id="example" class="table table-striped table-bordered"><!-- Beri id pada tag table untuk dideteksi javascript-->
    <thead>
    <tr class="active">
        <th>No</th><th>NIM</th><th>Nama</th><th>JK</th><th>Prodi id</th><th>Tahun Masuk</th><th>Rombel id</th><th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $nomor = 1;
    foreach($rows as $row){
        echo '<tr><td>'.$nomor.'</td>';
        echo '<td>'.$row['nim'].'</td>';
        echo '<td>'.$row['nama'].'</td>';
        echo '<td>'.$row['jk'].'</td>';
        echo '<td>'.$row['prodi_id'].'</td>';
        echo '<td>'.$row['thnmasuk'].'</td>';
        echo '<td>'.$row['rombel_id'].'</td>';
        echo '<td><a href="view_mahasiswa.php?id='.$row['nim']. '">View</a> |';
        echo '<a href="form_mahasiswa.php?id='.$row['nim']. '">Update</a></td>';
        echo '</tr>';
        $nomor++;
    }
    ?>
    

