$(function() {
	$("form[name='form_mahasiswa']").validate({
		rules: {
			nim: {
				required:true,
				maxlength:10,
			},
			nama: "required",
			jk: "required",
			prodi_id: "required",
			thnmasuk: "required",
			rombel_id: "required",
		},

		messages: {
			nim: {
				required: "NIM wajib diisi",
				maxlength: "maksimum 10 character!"
			},
			nama: "Nama wajib diisi!",
			jk: "pilih jenis kelamin wajib diisi!",
			prodi_id: "Prodi ID wajib diisi!",
			thnmasuk: "Tahun masuk wajib diisi",
			rombel_id: "Rombel ID wajib diisi!",
		},

		submitHandler: function(form){
			form.submit();
		}
	});
});
