<?php
    include_once 'top.php';
    require_once 'db/class_mahasiswa.php';
    //panggil file untuk operasi db
    //buat variabel utk menyimpan id
    //buat variabel untuk mengambil id
    $objMahasiswa = new Mahasiswa();
    $_id = $_GET['id'];
    $data = $objMahasiswa->findByID($_id);
?>
<!--Buat tampilan dengan tabel-->

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">NIM Mahasiswa '<?php echo
                $data['nim']?>'</h3> 
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered">
                <tr>
                <td class="active">Nama :</td><td><?php echo
                $data['nama']?></td>
                </tr>
                <tr>
                <td class="active">Jenis Kelamin :</td><td><?php
                echo $data['jk']?></td>
                </tr>
                <tr>
                <td class="active">Prodi ID :</td><td><?php echo
                $data['prodi_id']?></td>
                </tr>
                <tr>
                <td class="active">Tahun Masuk :</td><td><?php echo
                $data['thnmasuk']?></td>
                </tr>
                <td class="active">Rombel ID :</td><td><?php echo
                $data['rombel_id']?></td>
                </tr>
                <tr>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="panel-header">
    <a class="btn icon-btn btn-success" href="index.php#a">
    Back
    </a>
</div>

<?php
    include_once 'bottom.php';
?>
